#include "sudokuGame.h"
#include "sudokuPlayer.h"

int main(int argc, char **argv){
	SudokuGame s;
	SudokuPlayer p(SudokuPlayer::Strategy::MAX, true);
	SudokuPlayer q(SudokuPlayer::Strategy::MAX, false);

	int arr[81] = {0,0,0,0,0,0,0,0,1,0,0,4,0,6,0,2,0,8,0,7,0,3,2,0,4,0,0,9,0,0,0,1,8,0,0,0,0,0,5,0,0,0,6,0,0,0,0,0,5,4,0,0,0,9,0,0,8,0,3,7,0,4,0,6,0,9,0,8,0,3,0,0,1,0,0,0,0,0,0,0,0};
	// y wings

	// int arr[81] = {0,0,0,0,0,0,0,9,4,7,6,0,9,1,0,0,5,0,0,9,0,0,0,2,0,8,1,0,7,0,0,5,0,0,1,0,0,0,0,7,0,9,0,0,0,0,8,0,0,3,1,0,6,7,2,4,0,1,0,0,0,7,0,0,1,0,0,9,0,0,4,5,9,0,0,0,0,0,1,0,0};
	// x wing x2
	// xyz wing
	// x cycles x3
	// xy chain
	s.readGame(arr);
	if (argc == 2){
		std::vector<int> vinput;
		for (int counter=0; argv[1][counter] != 0; counter++ ){
			if (argv[1][counter] >= '1' || argv[1][counter] <= '9'){
				vinput.push_back(static_cast<int>(argv[1][counter] - '0'));
			} else {
				vinput.push_back(0);
			}
		}
		if (vinput.size() == 81){
			s.readGame(vinput);
		} else {
			std::cerr << "Error, input wrong, size: ";
			std::cout << vinput.size() << std::endl;
			return 1;
		}
	}

	p.playGame(s);
	if (s.isValid() && s.isGameDone()){
		std::cout << "Final:\n" << s;
	} else {
		std::cout << "All it can do:\n" << s;
	}
	std::cout << "\n" << s.exportGame();

	return 0;
}