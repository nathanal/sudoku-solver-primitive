#ifndef SUDOKU_GAME_H
#define SUDOKU_GAME_H

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>

class SudokuGame{
public:
	const int NULL_VALUE = -1;
private:
	int size;
	int squareSize;

	std::vector<std::vector<int>> solvedGrid;
	std::vector<std::vector<std::vector<bool>>> candidates;

	int cellsLeft;
	void resetBoard();
	bool validInd(int i) const;
public:
	SudokuGame(int s=9);
public:
	bool isGameDone() const;
	bool isValid() const;

	int getSize() const;
	int getSquareSize() const;
	int squareToPos1(int square, int pos) const;
	int squareToPos2(int square, int pos) const;
	int getSquare(int pos1, int pos2) const;
	int getSquareIndex(int pos1, int pos2) const;
	int getColumn(int pos1, int pos2) const;
	int getRow(int pos1, int pos2) const;

	int getCell(int pos1, int pos2, bool cartesian=true) const;

	bool isCandidate(int pos1, int pos2, int candidate, bool cartesian=true) const;
	
	void readGame(char const* fileName);
	void readGame(int* arr);
	void readGame(std::vector<int> arr);
	void switchCandidate(int pos1, int pos2, int candidate, bool switchValue, bool cartesian=true);
	void setNumber(int pos1, int pos2, int num, bool cartesian=true);

	std::string exportGame() const;
	friend std::ostream& operator<<(std::ostream& out, const SudokuGame& s );

};

#endif