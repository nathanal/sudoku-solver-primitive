#include "sudokuPlayer.h"

void SudokuPlayer::resetCandidates(SudokuGame& game){
	for (int pos1=0; pos1<size; pos1++){
		for (int pos2=0; pos2<size; pos2++){
			if (game.getCell(pos1, pos2) == game.NULL_VALUE){
				for (int candidate=0; candidate<size; candidate++){
					game.switchCandidate(pos1, pos2, candidate, true);
				}
			}
		}
	}
}

int SudokuPlayer::removeCandidateFromGroup(SudokuGame& game, int groupIndex, int candidate, Group group, std::vector<int> exceptions=std::vector<int>()){
	int nRemoved = 0;
	for (int i=0; i<size; i++){
		if (std::find(exceptions.begin(), exceptions.end(), i) != exceptions.end()){
			continue;
		}
		switch (group){
		case Group::ROW:
			if (game.isCandidate(groupIndex, i, candidate)){
				nRemoved++;
				game.switchCandidate(groupIndex, i, candidate, false);
			}
			break;
		case Group::COLUMN:
			if (game.isCandidate(i, groupIndex, candidate)){
				nRemoved++;
				game.switchCandidate(i, groupIndex, candidate, false);
			}
			break;
		case Group::SQUARE:
			if (game.isCandidate(groupIndex, i, candidate, false)){
				nRemoved++;
				game.switchCandidate(groupIndex, i, candidate, false, false);
			}
			break;
		}
	}
	return nRemoved;
}

void SudokuPlayer::removeCandidateFromSame(SudokuGame& game, int pos1, int pos2, int candidate){
	removeCandidateFromGroup(game, pos1, candidate, Group::ROW);
	removeCandidateFromGroup(game, pos2, candidate, Group::COLUMN);
	int square = game.getSquare(pos1, pos2);
	removeCandidateFromGroup(game, square, candidate, Group::SQUARE);
}

void SudokuPlayer::removeUnnecessaryCandidates(SudokuGame& game){
	for (int pos1=0; pos1 < size; pos1++){
		for (int pos2=0; pos2 < size; pos2++){
			int candidate = game.getCell(pos1, pos2);
			if (candidate != game.NULL_VALUE){
				// remove candidate from square, column, row
				removeCandidateFromSame(game, pos1, pos2, candidate);
			}
		}
	}
}

void SudokuPlayer::printMove(int pos1, int pos2, int candidate, bool isSet) const {
	if (isSet){
		std::cout << "Placed a " << candidate+1 << " at (" << pos1+1 << " " << pos2+1 << ")\n";
	} else {
		std::cout << "Removing " << candidate+1 << " from (" << pos1+1 << " " << pos2+1 << ")\n";
	}
	return;
}

void SudokuPlayer::printCandidates(SudokuGame& game) const {
	for (int candidate=0; candidate<9; candidate++){
		for (int i=0; i<9; i++){
			std::cout << "| ";
			for (int j=0; j<9; j++){
				if (game.getCell(i, j) == candidate){
					std::cout << candidate+1 << " ";
				} else {
					if (game.isCandidate(i, j, candidate)){
						std::cout << "x ";
					} else {
						std::cout << "  ";
					}						
				}
			}
			std::cout << "|\n";
		}
		std::cout << "\n";
	}
}

int SudokuPlayer::countCandidatesInCell(SudokuGame& game, int pos1, int pos2, Group group) const {
	int i = convertToNewGroupIndex(game, pos1, pos2, group);
	int j = convertToNewGroupPos(game, pos1, pos2, group);
	if (game.getCell(i, j) != game.NULL_VALUE){
		return 0;
	}
	int counter = 0;
	for (int candidate=0; candidate<size; candidate++){
		if (game.isCandidate(i, j, candidate)){
			counter++;
		}
	}
	return counter;
}

int SudokuPlayer::countCandidateInGroup(SudokuGame& game, int groupIndex, Group group, int candidate) const {
	int counter = 0;
	for (int i=0; i<size; i++){
		switch (group){
		case Group::ROW:
			if (game.isCandidate(groupIndex, i, candidate)){
				counter++;
			}
			break;
		case Group::COLUMN:
			if (game.isCandidate(i, groupIndex, candidate)){
				counter++;
			}
			break;
		case Group::SQUARE:
			if (game.isCandidate(groupIndex, i, candidate, false)){
				counter++;
			}
			break;
		}
	}
	return counter;
}

std::vector<int> SudokuPlayer::countCandidatesInGroup(SudokuGame& game, int groupIndex, Group group) const {
	std::vector<int> candidateCount;
	candidateCount.resize(size);
	for (int candidate=0; candidate<size; candidate++){
		candidateCount[candidate] = countCandidateInGroup(game, groupIndex, group, candidate);
	}
	return candidateCount;
}

std::vector<int> SudokuPlayer::getCandidatesInCell(SudokuGame& game, int pos1, int pos2, Group group) const {
	int i = convertToNewGroupIndex(game, pos1, pos2, group);
	int j = convertToNewGroupPos(game, pos1, pos2, group);
	std::vector<int> candidates;
	for (int candidate=0; candidate<size; candidate++){
		if (game.isCandidate(i, j, candidate)){
			candidates.push_back(candidate);
		}
	}
	return candidates;
}

std::vector<int> SudokuPlayer::getIndexOfCandidateByGroup(SudokuGame& game, int groupIndex, int candidate, Group group) const {
	std::vector<int> candidateIndexes;
	for (int i=0; i<size; i++){
		switch (group){
		case Group::ROW:
			if (game.isCandidate(groupIndex, i, candidate)){
				candidateIndexes.push_back(i);
			}
			break;
		case Group::COLUMN:
			if (game.isCandidate(i, groupIndex, candidate)){
				candidateIndexes.push_back(i);
			}
			break;
		case Group::SQUARE:
			if (game.isCandidate(groupIndex, i, candidate, false)){
				candidateIndexes.push_back(i);
			}
			break;
		}
	}
	return candidateIndexes;
}

bool SudokuPlayer::isIdenticalCell(SudokuGame& game, int a, int b, int i, int j, bool cartesian) const {
	if (!cartesian){
		int p1 = game.squareToPos1(a, b);
		int p2 = game.squareToPos2(a, b);
		a = p1;
		b = p2;
		p1 = game.squareToPos1(i, j);
		p2 = game.squareToPos2(i, j);
		i = p1;
		j = p2;
	}
	for (int candidate=0; candidate<size; candidate++){
		if (game.isCandidate(a, b, candidate) != game.isCandidate(i, j, candidate)){
			return false;
		}
	}
	return true;
}

std::vector<int> SudokuPlayer::findIdenticalInSameGroup(SudokuGame& game, int groupIndex, int index, Group group) const {
	std::vector<int> indexes;
	for (int i=0; i<size; i++){
		if (i == index){
			continue;
		}
		switch (group){
		case Group::ROW:
			if (isIdenticalCell(game, groupIndex, i, groupIndex, index)){
				indexes.push_back(i);
			}
			break;
		case Group::COLUMN:
			if (isIdenticalCell(game, i, groupIndex, index, groupIndex)){
				indexes.push_back(i);
			}
			break;
		case Group::SQUARE:
			if (isIdenticalCell(game, groupIndex, i, groupIndex, index, false)){
				indexes.push_back(i);
			}
			break;
		}
	}

	return indexes;
}

bool SudokuPlayer::methodHiddenSingles(SudokuGame& game){
	// look for a cell with only one candidate and isn't already solved
	for (int pos1=0; pos1<size; pos1++){
		for (int pos2=0; pos2<size; pos2++){
			if (game.getCell(pos1, pos2) == game.NULL_VALUE){
				if (countCandidatesInCell(game, pos1, pos2) == 1){
					int candidate = game.NULL_VALUE;
					for (int i=0; i<size; i++){
						if (game.isCandidate(pos1, pos2, i)){
							candidate=i;
							break;
						}
					}

					game.setNumber(pos1, pos2, candidate);
					if (showMove){ 
						std::cout << "Hidden Single found! \n";
						printMove(pos1, pos2, candidate);
					}
					return true;						
				}
			}
		}
	}
	return false;
}

bool SudokuPlayer::methodHelperNakedSets(SudokuGame& game, int nNaked, Group group){
	for (int groupIndex=0; groupIndex<size; groupIndex++){
		std::vector<int> indexes;
		for (int index=0; index<size; index++){
			int count=0;
			switch (group){
			case Group::ROW:

				count = countCandidatesInCell(game, groupIndex, index);
				break;
			case Group::COLUMN:
				count = countCandidatesInCell(game, index, groupIndex);
				break;
			case Group::SQUARE:
				count = countCandidatesInCell(game, 
										  game.squareToPos1(groupIndex, index), 
										  game.squareToPos2(groupIndex, index) );
				break;
			}
			if (count <= nNaked && count > 0){
				indexes.push_back(index);
			}
		}
		int nIndexes = indexes.size();
		if (nIndexes >= nNaked){
			std::vector<bool> curChecking; // this is taken from mitchnull's response at SE: https://stackoverflow.com/questions/9430568/generating-combinations-in-c
			curChecking.resize(nIndexes);
			std::fill(curChecking.begin(), curChecking.end(), false);
			std::fill(curChecking.end()-nNaked, curChecking.end(), true);
			do {
				std::vector<int> possCandidates;
				std::vector<int> posIndexes;
				for (int i=0; i<nIndexes; i++){
					if (curChecking[i]){
						posIndexes.push_back(indexes[i]);
						std::vector<int> curCandidates;
						curCandidates = getCandidatesInCell(game, groupIndex, indexes[i], group);
						for (auto candidate : curCandidates){
							if (std::find(possCandidates.begin(), possCandidates.end(), candidate) == possCandidates.end()){
								possCandidates.push_back(candidate);
							}
						}	
					}
				}
				if (static_cast<int>(possCandidates.size()) == nNaked){
					int nRemoved = 0;
					for (auto candidate : possCandidates){
						nRemoved += removeCandidateFromGroup(game, groupIndex, candidate, group, posIndexes);
					}
					if (nRemoved > 0){
						if (showMove){
							if (nNaked == 2){ std::cout << "Naked pair"; }
							else if (nNaked == 3){ std::cout << "Naked triplet"; }
							else { std::cout << "Naked set"; }
							std::cout << " found on " << group << " " <<  groupIndex+1 << "\n";
							std::cout << "Cells in set are: ";
							for (int i=0; i<nIndexes; i++){
								if (curChecking[i]){
									std::cout << indexes[i]+1;
									std::cout << " ";
								}
							}
							std::cout << "\n";

							std::cout << "Removing " << nRemoved << " candidates: ";
							for (int i=0; i<nNaked; i++){
								std::cout << possCandidates[i] + 1;
								if (i+1 != nNaked){
									std::cout << " ";
								} else {
									std::cout << "\n";
								}
							}
						}
						return true;
					}
				}

			} while (std::next_permutation(curChecking.begin(), curChecking.end()));
		}
	}
	return false;
}

bool SudokuPlayer::methodNakedSets(SudokuGame& game){
	int maxToCheck = size/2;
	for (int i=2; i<maxToCheck; i++){
		if (methodHelperNakedSets(game, i, Group::ROW) || 
			methodHelperNakedSets(game, i, Group::COLUMN) ||
			methodHelperNakedSets(game, i, Group::SQUARE)){
			return true;
		}		
	}
	return false;
}

bool SudokuPlayer::methodHelperNakedSingles(SudokuGame& game, Group group){
	for (int groupIndex=0; groupIndex<size; groupIndex++){
		std::vector<int> candidateCountRow = countCandidatesInGroup(game, groupIndex, group);
		for (int candidate=0; candidate<size; candidate++){
			if (candidateCountRow[candidate] == 1){
				int index = getIndexOfCandidateByGroup(game, groupIndex, candidate, group)[0];
				switch (group){
				case Group::ROW:
					game.setNumber(groupIndex, index, candidate);
					if (showMove){ 
						std::cout << "Naked single found on " << group << " " <<  groupIndex+1 << "\n";
						printMove(groupIndex, index, candidate);
					}
					break;
				case Group::COLUMN:
					game.setNumber(index, groupIndex, candidate);
					if (showMove){ 
						std::cout << "Naked single found on " << group << " " <<  groupIndex+1 << "\n";
						printMove(index, groupIndex, candidate);
					}
					break;
				case Group::SQUARE:
					game.setNumber(groupIndex, index, candidate, false);
					if (showMove){ 
						std::cout << "Naked single found on " << group << " " <<  groupIndex+1 << "\n";
						printMove(game.squareToPos1(groupIndex, index),
								  game.squareToPos2(groupIndex, index),
								  candidate);
					}
					break;
				}
				return true;
			}
		}
	}
	return false;
}

bool SudokuPlayer::methodNakedSingles(SudokuGame& game){
	// look for a candidate that is only represented once in a square/row/col
	if (methodHelperNakedSingles(game, Group::ROW) ||
		methodHelperNakedSingles(game, Group::COLUMN) ||
		methodHelperNakedSingles(game, Group::SQUARE)){
		return true;
	}
	return false;
}

bool SudokuPlayer::methodHelperHiddenSets(SudokuGame& game, int nHidden, Group group){
	for (int groupIndex=0; groupIndex<size; groupIndex++){
		std::vector<int> possCandidates;
		// initialise poss candidates with candidates that may form a hidden set
		std::vector<int> candidateCounts = countCandidatesInGroup(game, groupIndex, group);
		for (unsigned int i=0; i<candidateCounts.size(); i++){
			if (candidateCounts[i] <= nHidden && candidateCounts[i] != 0){
				possCandidates.push_back(i);
			}
		}
		if (possCandidates.size() < static_cast<unsigned int>(nHidden)){
			continue;
		}

		// now generate a mask to look at only nHidden possCandidates at a time
		// then for each mask, check if the nHidden candidates form a hidden set.
		std::vector<int> candidateMask;
		candidateMask.resize(possCandidates.size());
		std::fill(candidateMask.begin(), candidateMask.end(), false);
		std::fill(candidateMask.begin(), candidateMask.begin()+nHidden, true);
		do {
			std::vector<int> possCandidatesRefined;
			for (unsigned int i=0; i<possCandidates.size(); i++){
				if (candidateMask[i]){
					possCandidatesRefined.push_back(possCandidates[i]);
				}
			}

			std::vector<bool> cells;
			cells.resize(size);
			std::fill(cells.begin(), cells.end(), false);
			for (unsigned int i=0; i<possCandidatesRefined.size(); i++){
				std::vector<int> curCells = getIndexOfCandidateByGroup(game, groupIndex, possCandidatesRefined[i], group);
				for (auto cell : curCells){
					cells[cell] = true;
				}
			}

			std::vector<int> cellIndexes; // contains index of cells that hold the candidates we are looking at
			for (unsigned int i=0; i<cells.size(); i++){
				if (cells[i]){
					cellIndexes.push_back(i);
				}
			}

			if (cellIndexes.size() == static_cast<unsigned int>(nHidden)){
				// alright, a hidden set has been found.
				// delete all not possCandidates from cell[i]=true
				
				int nRemoved = 0;
				for (auto index : cellIndexes){
					std::vector<bool> isAPotentialCandidate;
					isAPotentialCandidate.resize(size);
					std::fill(isAPotentialCandidate.begin(), isAPotentialCandidate.end(), false);
					for (auto candidate : possCandidatesRefined){
						isAPotentialCandidate[candidate] = true;
					}
					for (int i=0; i<size; i++){
						if (!isAPotentialCandidate[i]){
							switch (group){
							case Group::ROW:
								if (game.isCandidate(groupIndex, index, i)){
									game.switchCandidate(groupIndex, index, i, false);
									nRemoved++;
								}
								break;
							case Group::COLUMN:
								if (game.isCandidate(index, groupIndex, i)){
									game.switchCandidate(index, groupIndex, i, false);
									nRemoved++;
								}
								break;
							case Group::SQUARE:
								if (game.isCandidate(groupIndex, index, i, false)){
									game.switchCandidate(groupIndex, index, i, false, false);
									nRemoved++;
								}
								break;
							}
							
						}
					}
				}
				if (nRemoved > 0){
					if (showMove){
						std::cout << "Hidden set found on " << group << " " << groupIndex+1 << "\n";
						std::cout << "Cells in set are: ";
						for (unsigned int i=0; i<cellIndexes.size(); i++){
							std::cout << cellIndexes[i]+1;
							std::cout << " ";
						}
						std::cout << "\n";

						std::cout << "Candidates: ";
						for (unsigned int i=0; i<possCandidates.size(); i++){
							if (candidateMask[i]){
								std::cout << possCandidates[i] + 1;
								std::cout << " ";
							}
						}
						std::cout << "\n";
					}
					return true;		
				}
			}
		} while (std::prev_permutation(candidateMask.begin(), candidateMask.end()));
	}
	return false;
}

bool SudokuPlayer::methodHiddenSets(SudokuGame& game){
	int maxToCheck = size/2;
	for (int i=2; i<maxToCheck; i++){
		if (methodHelperHiddenSets(game, i, Group::ROW) || 
			methodHelperHiddenSets(game, i, Group::COLUMN) ||
			methodHelperHiddenSets(game, i, Group::SQUARE)){
			return true;
		}		
	}
	return false;
}

SudokuPlayer::Group SudokuPlayer::otherGroupShared(SudokuGame& game, int a, int b, int i, int j, Group group) const {
	switch (group){
	case Group::SQUARE:
		if (game.squareToPos1(a, b) == game.squareToPos1(i, j)){
			return Group::ROW;
		} else if (game.squareToPos2(a, b) == game.squareToPos2(i, j)){
			return Group::COLUMN;
		}
		break;
	case Group::COLUMN:
		if (b == j){
			return Group::ROW;
		}
		std::swap(a, b);
		std::swap(i, j);
	case Group::ROW:
		if (game.getSquare(a, b) == game.getSquare(i, j)){
			return Group::SQUARE;
		} else if (b == j){
			return Group::COLUMN;
		}
		break;
	}
	return group;
}

int SudokuPlayer::convertToNewGroupIndex(SudokuGame& game, int groupIndex, int index, Group group, Group newGroup) const {
	if (group == newGroup){
		return groupIndex;
	} else if (group != Group::SQUARE && newGroup != Group::SQUARE){
		return index;
	}
	switch (group){
	case Group::SQUARE:
		if (newGroup == Group::COLUMN){
			return game.squareToPos2(groupIndex, index);
		} else {
			return game.squareToPos1(groupIndex, index);
		}
		break;
	case Group::COLUMN:
		std::swap(groupIndex, index);
	case Group::ROW:
	default:
		return game.getSquare(groupIndex, index);
		break;
	}
}

int SudokuPlayer::convertToNewGroupPos(SudokuGame& game, int groupIndex, int index, Group group, Group newGroup) const {
	if (group == newGroup){
		return index;
	} else if (group != Group::SQUARE && newGroup != Group::SQUARE){
		return groupIndex;
	}
	switch (group){
	case Group::SQUARE:
		if (newGroup == Group::COLUMN){
			return game.squareToPos1(groupIndex, index);
		} else {
			return game.squareToPos2(groupIndex, index);
		}
		break;
	case Group::COLUMN:
		std::swap(groupIndex, index);
	case Group::ROW:
	default:
		return game.getSquareIndex(groupIndex, index);
		break;
	}
}

bool SudokuPlayer::methodHelperIntersectionRemoval(SudokuGame& game, Group group){
	for (int groupIndex=0; groupIndex<size; groupIndex++){
		for (int candidate=0; candidate<size; candidate++){
			std::vector<int> cells = getIndexOfCandidateByGroup(game, groupIndex, candidate, group);
			if (cells.size() <= static_cast<unsigned int>(game.getSquareSize()) && cells.size() > 1){
				Group groupShared = otherGroupShared(game, groupIndex, cells[0], groupIndex, cells[1], group);
				if (groupShared != group){
					for (unsigned int i=2; i<cells.size(); i++){
						Group curGroupShared = otherGroupShared(game, groupIndex, cells[0], groupIndex, cells[i], group);
						if (curGroupShared != groupShared){
							groupShared = group; // signify that it isn't a match doc :(
							break;
						}
					}					
				}
				if (groupShared == group){
					// no intersection here.
					continue;
				}
				int newGroupIndex = convertToNewGroupIndex(game, groupIndex, cells[0], group, groupShared);
				std::vector<int> translatedCells;
				for (unsigned int i=0; i<cells.size(); i++){
					translatedCells.push_back( convertToNewGroupPos(game, groupIndex, cells[i], group, groupShared) );
				}
				int nRemoved = 0;
				nRemoved += removeCandidateFromGroup(game, newGroupIndex, candidate, groupShared, translatedCells);
				if (nRemoved > 0){
					// removed stuff
					if (showMove){
						std::cout << "Intersection found on " << group << " " << groupIndex+1 << " and ";
						std::cout << groupShared << " " << newGroupIndex+1 << " regarding candidate: " << candidate+1 << "\n";
						std::cout << "Removed extraneous candidate from " << groupShared << " " << newGroupIndex+1 << "\n";
					}
					return true;
				}
			}
		}	
	}
	
	return false;
}
bool SudokuPlayer::methodIntersectionRemoval(SudokuGame& game){
	if (methodHelperIntersectionRemoval(game, Group::ROW) || 
		methodHelperIntersectionRemoval(game, Group::COLUMN) ||
		methodHelperIntersectionRemoval(game, Group::SQUARE)){
		return true;
	}
	return false;
}

bool SudokuPlayer::methodHelperXWing(SudokuGame& game, int candidate, Group group, int length){
	if (group == Group::SQUARE){ return false; } // use pointed pairs instead
	std::vector<std::vector<int>> candidateIndexByGroupIndex;
	candidateIndexByGroupIndex.resize(size);
	std::vector<int> targetGroupIndexes;

	for (int groupIndex=0; groupIndex<size; groupIndex++){
		int nCandidates = 0;
		for (int index = 0; index<size; index++){
			bool isAvailable = (group == Group::ROW) ? game.isCandidate(groupIndex, index, candidate) : game.isCandidate(index, groupIndex, candidate);
			if (isAvailable){
				nCandidates++;
				candidateIndexByGroupIndex[groupIndex].push_back(index);
			}
		}
		if (nCandidates >= 2 && nCandidates <= length){
			targetGroupIndexes.push_back(groupIndex);
		}
	}

	if (static_cast<int>(targetGroupIndexes.size()) < length){ return false; }
	bool changed = false;
	std::vector<bool> isChecked;
	isChecked.resize(targetGroupIndexes.size());
	std::fill(isChecked.begin(), isChecked.end(), false);
	std::fill(isChecked.begin(), isChecked.begin()+length, true);

	do {
		std::vector<int> occurences;
		std::vector<int> toPreserve;
		for (int i=0; i<static_cast<int>(targetGroupIndexes.size()); i++){
			if (isChecked[i]){
				toPreserve.push_back(targetGroupIndexes[i]);
				for (auto o : candidateIndexByGroupIndex[targetGroupIndexes[i]]){
					if (std::find(occurences.begin(), occurences.end(), o) == occurences.end()){
						occurences.push_back(o);
					}
				}
			}
		}
		if (static_cast<int>(occurences.size()) == length){
			for (auto otherGroupIndex : occurences){
				for (int j=0; j<size; j++){
					if (std::find(toPreserve.begin(), toPreserve.end(), j) != toPreserve.end()){ continue; }
					if (group == Group::COLUMN && (game.isCandidate(otherGroupIndex, j, candidate))){
						game.switchCandidate(otherGroupIndex, j, candidate, false);
						if (showMove){
							if (!changed){
								if (length == 2){
									std::cout << "XWing";
								} else if (length == 3){
									std::cout << "Swordfish";
								} else {
									std::cout << "Jellyfish";
								}
								std::cout << " found on " << group << "s: ";
								for (int temp=0; temp<targetGroupIndexes.size(); temp++){
									if (isChecked[temp]){
										std::cout << targetGroupIndexes[temp]+1 << " ";
									}
								}
								std::cout << "with candidate " << candidate+1 << "\n";
							}
							printMove(otherGroupIndex, j, candidate, false);
						}
						changed = true;
					} else if (group == Group::ROW && (game.isCandidate(j, otherGroupIndex, candidate))){
						game.switchCandidate(j, otherGroupIndex, candidate, false);
						if (showMove){
							if (!changed){
								if (length == 2){
									std::cout << "XWing";
								} else if (length == 3){
									std::cout << "Swordfish";
								} else {
									std::cout << "Jellyfish";
								}
								std::cout << " found on " << group << "s: ";
								for (int temp=0; temp<targetGroupIndexes.size(); temp++){
									if (isChecked[temp]){
										std::cout << targetGroupIndexes[temp]+1 << " ";
									}
								}
								std::cout << "with candidate " << candidate+1 << "\n";
							}
							printMove(j, otherGroupIndex, candidate, false);
						}
						changed = true;
					}
				}
			}
		}

	} while (prev_permutation(isChecked.begin(), isChecked.end()));

	return changed;
}

bool SudokuPlayer::methodXWing(SudokuGame& game){
	for (int candidate=0; candidate<size; candidate++){
		if (methodHelperXWing(game, candidate, Group::ROW) || (methodHelperXWing(game, candidate, Group::COLUMN))){
			return true;
		}
	}
	return false;
}

bool SudokuPlayer::methodSwordfish(SudokuGame& game){
	for (int candidate=0; candidate<size; candidate++){
		if (methodHelperXWing(game, candidate, Group::ROW, 3) || (methodHelperXWing(game, candidate, Group::COLUMN, 3))){
			return true;
		}
	}
	return false;
}

bool SudokuPlayer::methodJellyfish(SudokuGame& game){
	for (int candidate=0; candidate<size; candidate++){
		if (methodHelperXWing(game, candidate, Group::ROW, 4) || (methodHelperXWing(game, candidate, Group::COLUMN, 4))){
			return true;
		}
	}
	return false;
}


bool SudokuPlayer::methodHelperYWing(SudokuGame& game, bool isXYZWing){
	std::vector<Group> allGroups;
	allGroups.push_back(Group::ROW);
	allGroups.push_back(Group::COLUMN);
	allGroups.push_back(Group::SQUARE);

	int hingeSize = isXYZWing ? 3 : 2;
	for (int i=0; i<size; i++){
		for (int j=0; j<size; j++){
			if (countCandidatesInCell(game, i, j) != hingeSize){ continue; }
			std::vector<int> candidates = getCandidatesInCell(game, i, j);
			for (int g=0; g < static_cast<int>(allGroups.size()) - 1; g++){
				int gIndex = convertToNewGroupIndex(game, i, j, Group::ROW, allGroups[g]);
				int gPos = convertToNewGroupPos(game, i, j, Group::ROW, allGroups[g]);
				for (int gPos2=0; gPos2<size; gPos2++){
					if (gPos == gPos2){ continue; }
					int indexG = convertToNewGroupIndex(game, gIndex, gPos2, allGroups[g], Group::ROW);
					int posG = convertToNewGroupPos(game, gIndex, gPos2, allGroups[g], Group::ROW);

					if (countCandidatesInCell(game, indexG, posG) != 2){ continue; }
					std::vector<int> candidates2 = getCandidatesInCell(game, indexG, posG);

					int toRemoveCandidate = game.NULL_VALUE;
					int otherCandidate = game.NULL_VALUE;
					if (!isXYZWing){
						if (candidates2[0] == candidates[0]){
							toRemoveCandidate = candidates2[1];
							otherCandidate = candidates[1];
						} else if (candidates2[1] == candidates[0]){
							toRemoveCandidate = candidates2[0];
							otherCandidate = candidates[1];
						} else if (candidates2[0] == candidates[1]){
							toRemoveCandidate = candidates2[1];
							otherCandidate = candidates[0];
						} else if (candidates2[1] == candidates[1]){
							toRemoveCandidate = candidates2[0];
							otherCandidate = candidates[0];
						} else { continue; }
					} else if ((std::find(candidates.begin(), candidates.end(), candidates2[0]) == candidates.end()) || 
							   (std::find(candidates.begin(), candidates.end(), candidates2[1]) == candidates.end())){
						continue;
					}

					Group other = otherGroupShared(game, gIndex, gPos, gIndex, gPos2, allGroups[g]);
					for (int h=0; h < static_cast<int>(allGroups.size()); h++){
						if (allGroups[h] == other){ continue; }
						int hIndex = convertToNewGroupIndex(game, i, j, Group::ROW, allGroups[h]);
						int hPos = convertToNewGroupPos(game, i, j, Group::ROW, allGroups[h]);
						for (int hPos2=0; hPos2<size; hPos2++){
							if (hPos == hPos2){ continue; }
							int indexH = convertToNewGroupIndex(game, hIndex, hPos2, allGroups[h], Group::ROW);
							int posH = convertToNewGroupPos(game, hIndex, hPos2, allGroups[h], Group::ROW);

							if (countCandidatesInCell(game, indexH, posH) != 2){ continue; }
							if (posH == posG && indexH == indexG){ continue; }

							std::vector<int> candidates3 = getCandidatesInCell(game, indexH, posH);
							bool match = false;
							if ((!isXYZWing) && ((candidates3[0] == otherCandidate && candidates3[1] == toRemoveCandidate)
											|| (candidates3[1] == otherCandidate && candidates3[0] == toRemoveCandidate))){
								match = true;
							} else if (isXYZWing && (std::find(candidates.begin(), candidates.end(), candidates3[0]) != candidates.end()) && 
									               (std::find(candidates.begin(), candidates.end(), candidates3[1]) != candidates.end())){
								if ((std::find(candidates2.begin(), candidates2.end(), candidates3[0]) != candidates2.end()) && 
									(std::find(candidates2.begin(), candidates2.end(), candidates3[1]) != candidates2.end())){
									match = false;
								} else {
									match = true;
								}
								if ((std::find(candidates2.begin(), candidates2.end(), candidates3[0]) != candidates2.end())){
									toRemoveCandidate = candidates3[0];
								} else {
									toRemoveCandidate = candidates3[1];
								}
							}
							if (match){
								// found YWing centred at i, j
								// with cell indexH, posH and cell indexG, posG
								// remove candidate toRemoveCandidate from all cells not i, j, sharing a group with both H and G
								bool removed = false;
								for (auto group : allGroups){
									int hGroupIndex = convertToNewGroupIndex(game, indexH, posH, Group::ROW, group);
									int hGroupPos = convertToNewGroupPos(game, indexH, posH, Group::ROW, group);
									for (int pos = 0; pos<size; pos++){
										if (pos == hGroupPos){ continue; }
										int x = 0;
										int y = 0;
										switch (group){
											case (Group::ROW):
												x = hGroupIndex;
												y = pos;
												break;
											case (Group::COLUMN):
												y = hGroupIndex;
												x = pos;
												break;
											case (Group::SQUARE):
												x = convertToNewGroupIndex(game, hGroupIndex, pos, group, Group::ROW);
												y = convertToNewGroupPos(game, hGroupIndex, pos, group, Group::ROW);
												break;
										}
										if (x == indexG && y == posG){ continue; }
										if (x == i && y == j){ continue; }
										if (x != indexG && otherGroupShared(game, x, y, indexG, posG, Group::ROW) == Group::ROW){ continue; }
										if (isXYZWing && x != i && otherGroupShared(game, x, y, i, j, Group::ROW) == Group::ROW){ continue; }
										if (game.isCandidate(x, y, toRemoveCandidate)){
											if (showMove){
												if (!removed){
													if (isXYZWing){ std::cout << "XYZ "; } else { std::cout << "XY "; }
													std::cout << "Wing found with the hinge at (" << i+1 << ", " << j+1 << ")\n";
													std::cout << "Wings: (" << indexG+1 << ", " << posG+1 << "), (" << indexH+1 << ", " << posH+1 << ")\n";
												}
												std::cout << "Removing " << toRemoveCandidate+1 << " from (" << x+1 << ", " << y+1 << ")\n";
											}
											removed = true;
											game.switchCandidate(x, y, toRemoveCandidate, false);
										}
									}
								}
								if (removed){
									return true;
								}
							}
						}
					}
				}
			}
		}
	}
	return false;
}

bool SudokuPlayer::methodYWing(SudokuGame& game){
	return methodHelperYWing(game);
}

bool SudokuPlayer::methodXYZWing(SudokuGame& game){
	return methodHelperYWing(game, true);
}

bool SudokuPlayer::methodBUG(SudokuGame& game){
	int bugX = game.NULL_VALUE;
	int bugY = game.NULL_VALUE;
	for (int i=0; i<size; i++){
		for (int j=0; j<size; j++){
			if (countCandidatesInCell(game, i, j) > 2){
				if (bugX != game.NULL_VALUE){
					return false;
				} else {
					bugX = i;
					bugY = j;
				}
			}
		}
	}
	if (bugX == game.NULL_VALUE){
		return false;
	}
	// if we are here, then the BUG is real
	std::vector<int> bugCandidates = getCandidatesInCell(game, bugX, bugY);
	std::vector<int> groupCandidateCount;
	groupCandidateCount = countCandidatesInGroup(game, bugX, Group::ROW);
	for (std::vector<int>::iterator candidate=bugCandidates.begin(); candidate != bugCandidates.end(); candidate++){
		if (groupCandidateCount[(*candidate)] == 2){
			candidate--;
			bugCandidates.erase(candidate+1);
		}
	}
	groupCandidateCount = countCandidatesInGroup(game, bugY, Group::COLUMN);
	for (std::vector<int>::iterator candidate=bugCandidates.begin(); candidate != bugCandidates.end(); candidate++){
		if (groupCandidateCount[(*candidate)] == 2){
			candidate--;
			bugCandidates.erase(candidate+1);
		}
	}
	groupCandidateCount = countCandidatesInGroup(game, game.getSquare(bugX, bugY), Group::SQUARE);
	for (std::vector<int>::iterator candidate=bugCandidates.begin(); candidate != bugCandidates.end(); candidate++){
		if (groupCandidateCount[(*candidate)] == 2){
			candidate--;
			bugCandidates.erase(candidate+1);
		}
	}
	if (bugCandidates.size() == 1){
		game.setNumber(bugX, bugY, bugCandidates.front());
		std::cout << "BUG found at (" << bugX+1 << ", " << bugY+1 << ")\n";
		if (showMove){
			printMove(bugX, bugY, bugCandidates.front());
		}
		return true;
	} else {
		std::cerr << "BUG method detected an abnormal hint. Multiple candidates lead to a bi-value universal grave" << std::endl;
		std::cerr << bugCandidates.size() << "\n";
		return false;
	}
}

bool SudokuPlayer::methodRemotePairs(SudokuGame& game){
	std::vector<Group> allGroups;
	allGroups.push_back(Group::ROW);
	allGroups.push_back(Group::COLUMN);
	allGroups.push_back(Group::SQUARE);

	std::vector<std::pair<int, int>> candidatePair;
	std::vector<std::vector<std::pair<int, int>>> posByCandidatePair;
	for (int i=0; i<size; i++){
		for (int j=0; j<size; j++){
			if (countCandidatesInCell(game, i, j) == 2){
				std::vector<int> candidateBuffer = getCandidatesInCell(game, i, j);
				std::pair<int, int> candidates = std::make_pair(candidateBuffer.front(), candidateBuffer.back());
				bool inserted = false;
				for (int index=0; index<candidatePair.size(); index++){
					if (candidates == candidatePair[index]){
						inserted = true;
						posByCandidatePair[index].push_back(std::make_pair(i, j));
					}
				}
				if (!inserted) {
					candidatePair.push_back(candidates);
					posByCandidatePair.resize(posByCandidatePair.size()+1);
					posByCandidatePair.back().push_back(std::make_pair(i, j));
				}
			}
		}
	}
	for (int considering=0; considering<candidatePair.size(); considering++){
		if (posByCandidatePair[considering].size() > 3){
			int nIdentical = posByCandidatePair[considering].size();
			std::vector<int> minDistance;
			minDistance.resize(nIdentical);
			for (int i=0; i<nIdentical; i++){
				std::fill(minDistance.begin(), minDistance.end(), game.NULL_VALUE);
				minDistance[i] = 0;
				// calculate minDistince to posByCandidatePair[i][i] for each cell
				bool distanceUpdated;
				do {
					distanceUpdated = false;
					for (int j=0; j<nIdentical; j++) {
						for (int k=0; k<nIdentical; k++){
							if (k == j || (minDistance[k] == game.NULL_VALUE)){ continue; }
							if ((posByCandidatePair[considering][j].first == posByCandidatePair[considering][k].first) ||
							   (otherGroupShared(game, posByCandidatePair[considering][j].first, posByCandidatePair[considering][j].second,
								   					posByCandidatePair[considering][k].first, posByCandidatePair[considering][k].second) != Group::ROW)){
								if (minDistance[j] == game.NULL_VALUE){
									minDistance[j] = minDistance[k]+1;
									distanceUpdated = true;
								} else if (minDistance[k]+1 < minDistance[j]){
									minDistance[j] = minDistance[k]+1;
									distanceUpdated = true;
								}
								
							}

						}
					}
				} while (distanceUpdated);
				for (int j=0; j<nIdentical; j++){
					if (minDistance[j] != game.NULL_VALUE && minDistance[j] > 1 && minDistance[j] % 2 == 1){
						// i and j form a remote pair. 
						std::pair<int, int> candidate = candidatePair[considering];
						std::pair<int, int> cellA = posByCandidatePair[considering][i];
						std::pair<int, int> cellB = posByCandidatePair[considering][j];
						std::vector<std::pair<int, int>> exclusions = posByCandidatePair[considering];
						// check all in same group as cellA and cellB
						bool changed = false;
						for (auto group : allGroups){
							int groupIndex = convertToNewGroupIndex(game, cellA.first, cellA.second, Group::ROW, group);
							for (int index = 0; index<size; index++){
								std::pair<int, int> cur = std::make_pair(
															convertToNewGroupIndex(game, groupIndex, index, group, Group::ROW), 
															convertToNewGroupPos(game, groupIndex, index, group, Group::ROW));
								if ((game.isCandidate(cur.first, cur.second, candidate.first) || game.isCandidate(cur.first, cur.second, candidate.second))
									&& (std::find(exclusions.begin(), exclusions.end(), cur) == exclusions.end()) ){
									if (otherGroupShared(game, cur.first, cur.second, cellB.first, cellB.second) == Group::ROW && cur.first != cellB.first){ continue; }
									game.switchCandidate(cur.first, cur.second, candidate.second, false);
									game.switchCandidate(cur.first, cur.second, candidate.first, false);
									if (showMove){
										if (!changed){
											std::cout << "Remote pair found with candidates " << candidate.first+1 << " and " << candidate.second+1 << "\n";
											std::cout << "Cells: ";
											for (auto e : exclusions){
												std::cout << "(" << e.first+1 << ", " << e.second+1 << ") ";
											}
											std::cout << "\n";
										}
										std::cout << "Removing the candidates from (" << cur.first+1 << ", " << cur.second+1 << ")\n";
									}
									changed = true;
								}
							}							
						}
						if (changed){
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}

bool SudokuPlayer::methodHelper3DMedusa(SudokuGame& game, int pos1, int pos2, int startingCandidate, bool fixedCandidate){
	std::vector<Group> allGroups;
	allGroups.push_back(Group::ROW);
	allGroups.push_back(Group::COLUMN);
	allGroups.push_back(Group::SQUARE);

	std::vector<std::pair<int, int>> cells;
	cells.push_back(std::make_pair(pos1, pos2));
	std::vector<std::pair<int, int>> colouring;
	colouring.push_back(std::make_pair(startingCandidate, game.NULL_VALUE));
	if ((!fixedCandidate) && countCandidatesInCell(game, pos1, pos2) == 2){
		for (int candidate=0; candidate<size; candidate++){
			if (candidate != startingCandidate && game.isCandidate(pos1, pos2, candidate)){
				colouring.back().second = candidate;
			}
		}
	}
	bool updated;
	bool invalidColouring = false;
	bool invalidIsFirst;
	do {
		updated = false;
		int nInChain = cells.size();
		for (int i=0; i<nInChain; i++){
			bool nextIsFirst;
			int candidate;
			for (int nullCounter=0; nullCounter<2; nullCounter++){
				if (nullCounter == 0){
					nextIsFirst = false;
					candidate = colouring[i].first;
				} else {
					nextIsFirst = true;
					candidate = colouring[i].second;
				}
				if (candidate == game.NULL_VALUE){
					continue;
				}
				for (auto group : allGroups){
					int groupIndex = convertToNewGroupIndex(game, cells[i].first, cells[i].second, Group::ROW, group);
					int groupPos = convertToNewGroupPos(game, cells[i].first, cells[i].second, Group::ROW, group);
					if (countCandidateInGroup(game, groupIndex, group, candidate) == 2){
						int otherGroupPos;
						// initialise otherGroupPos
						{
							std::vector<int> otherIndexBuffer = getIndexOfCandidateByGroup(game, groupIndex, candidate, group);
							if (otherIndexBuffer[0] == groupPos){
								otherGroupPos = otherIndexBuffer[1];
							} else {
								otherGroupPos = otherIndexBuffer[0];
							}
						}
						std::pair<int, int> curCell;
						curCell.first = convertToNewGroupIndex(game, groupIndex, otherGroupPos, group, Group::ROW);
						curCell.second = convertToNewGroupPos(game, groupIndex, otherGroupPos, group, Group::ROW);

						int extraCandidate = game.NULL_VALUE;
						if ((!fixedCandidate) && countCandidatesInCell(game, curCell.first, curCell.second) == 2){
							std::vector<int> extraCandidateBuffer = getCandidatesInCell(game, curCell.first, curCell.second);
							if (extraCandidateBuffer[0] == candidate){
								extraCandidate = extraCandidateBuffer[1];
							} else {
								extraCandidate = extraCandidateBuffer[0];
							}
						}

						int cellsIndex = std::find(cells.begin(), cells.end(), curCell) - cells.begin();
						if (cellsIndex == cells.size()){
							updated = true;
							cells.push_back(curCell);
							if (nextIsFirst){
								colouring.push_back(std::make_pair(candidate, extraCandidate));
							} else {
								colouring.push_back(std::make_pair(extraCandidate, candidate));
							}
						} else {
							int *oldColouring = (nextIsFirst) ? &colouring[cellsIndex].first : &colouring[cellsIndex].second;
							if (*oldColouring == candidate){
								continue;
							} else if (*oldColouring == game.NULL_VALUE){
								updated = true;
								*oldColouring = candidate;
							} else {
								updated = true;
								invalidColouring = true;
								invalidIsFirst = nextIsFirst;
							}
						}
					}
				}
			}
		}
	} while (updated && !invalidColouring);

	int nCells = cells.size();

	// std::cout << "Testing " << (fixedCandidate ? "Simple Colouring" : "3D Medusa") << "\n";
	// for (int i=0;  i<nCells;i++){
	// 	std::cout << "(" << cells[i].first+1 << ", " << cells[i].second+1 << ") -> " << colouring[i].first+1 << ", " << colouring[i].second+1 << " of ";
	// 	for (int j=0; j<size; j++){
	// 		if (game.isCandidate(cells[i].first, cells[i].second, j)){
	// 			std::cout << j+1 << " ";
	// 		}
	// 	}
	// 	std::cout << "\n";
	// }
	// std::cout << "\n";

	// check for invalid colouring outsite of same cell
	if (!invalidColouring){
		for (int cellA=0; cellA < nCells; cellA++){
			for (int cellB=cellA+1; cellB < nCells; cellB++){
				if (cells[cellA].first == cells[cellB].first || otherGroupShared(game, cells[cellA].first, cells[cellA].second, cells[cellB].first, cells[cellB].second) != Group::ROW){
					if (colouring[cellA].first == colouring[cellB].first && colouring[cellA].first != game.NULL_VALUE){
						invalidColouring = true;
						invalidIsFirst = true;
						break;
					} else if (colouring[cellA].second == colouring[cellB].second && colouring[cellA].second != game.NULL_VALUE){
						invalidColouring = true;
						invalidIsFirst = false;
						break;
					}
				}
			}
			if (invalidColouring){
				break;
			}
		}
	}
	// check if a colouring will deplete another cell.
	// 1. cell is not in cells
	// 2. all of cell 's candidates are seen by one colour
	// 3. thus that colour is invalid
	if (!invalidColouring){
		std::pair<int, int> curCell;
		for (int i=0; i<size; i++){
			for (int j=0; j<size; j++){
				curCell.first = i; curCell.second = j;
				if (std::find(cells.begin(), cells.end(), curCell) != cells.end() || countCandidatesInCell(game, i, j) == 0){ continue; }
				std::vector<int> candidates = getCandidatesInCell(game, i, j);
				// for all that are visible to curCell
				for (int cellIndex=0; cellIndex<nCells; cellIndex++){
					if (cells[cellIndex].first == curCell.first ||
						otherGroupShared(game, cells[cellIndex].first, cells[cellIndex].second, curCell.first, curCell.second) != Group::ROW){
						std::vector<int>::iterator candidateIndex = std::find(candidates.begin(), candidates.end(), colouring[cellIndex].first);
						if (candidateIndex != candidates.end()){
							candidates.erase(candidateIndex);
						}
					}
				}
				if (candidates.empty()){
					invalidColouring = true;
					invalidIsFirst = true;
					break;
				}

				// repeat but with colouring[...].second
				candidates = getCandidatesInCell(game, i, j);
				// for all that are visible to curCell
				for (int cellIndex=0; cellIndex<nCells; cellIndex++){
					if (cells[cellIndex].first == curCell.first ||
						otherGroupShared(game, cells[cellIndex].first, cells[cellIndex].second, curCell.first, curCell.second) != Group::ROW){
						std::vector<int>::iterator candidateIndex = std::find(candidates.begin(), candidates.end(), colouring[cellIndex].second);
						if (candidateIndex != candidates.end()){
							candidates.erase(candidateIndex);
						}
					}
				}
				if (candidates.empty()){
					invalidColouring = true;
					invalidIsFirst = false;
					break;
				}
			}
		}
	}

	bool changed = invalidColouring;

	if (invalidColouring){
		if (showMove){
			if (fixedCandidate){ std::cout << "Simple Colouring"; }
			else { std::cout << "3D Medusa"; }
			std::cout << " with invalid colouring detected involving cells ";
			for (auto cell : cells){
				std::cout << "(" << cell.first+1 << ", " << cell.second+1 << ") ";
			}
			std::cout << "\n";
		}
		for (int i=0; i<nCells; i++){
			if (invalidIsFirst){
				std::swap(colouring[i].first, colouring[i].second);
			}
			if (colouring[i].first != game.NULL_VALUE){
				game.setNumber(cells[i].first, cells[i].second, colouring[i].first);
				if (showMove){
					printMove(cells[i].first, cells[i].second, colouring[i].first);
				}
			} else {
				game.switchCandidate(cells[i].first, cells[i].second, colouring[i].second, false);
				if (showMove){
					printMove(cells[i].first, cells[i].second, colouring[i].second, false);
				}
			}
		}
	} else {
		// check other rules

		// removing candidate from cell with both colourings on already
		for (int i=0; i<nCells; i++){
			if (colouring[i].first != game.NULL_VALUE && colouring[i].second != game.NULL_VALUE && 
				countCandidatesInCell(game, cells[i].first, cells[i].second) != 2){
				for (int c = 0; c<size; c++){
					if (c == colouring[i].first || c == colouring[i].second){ continue; }
					if (game.isCandidate(cells[i].first, cells[i].second, c)){
						game.switchCandidate(cells[i].first, cells[i].second, c, false);
						if (showMove){
							if (!changed){
								if (fixedCandidate){ std::cout << "Simple Colouring"; }
								else { std::cout << "3D Medusa"; }
								std::cout << " detected involving cells ";
								for (auto cell : cells){
									std::cout << "(" << cell.first+1 << ", " << cell.second+1 << ") ";
								}
								std::cout << "\n";			
							}
							printMove(cells[i].first, cells[i].second, c, false);
						}
						changed = true;
					}
				}
			}
		}

		// removing candidate from cells that see both colourings
		std::pair<int, int> curCell;
		for (int i=0; i<size; i++){
			for (int j=0; j<size; j++){
				curCell.first = i; curCell.second = j;
				if (std::find(cells.begin(), cells.end(), curCell) != cells.end() || countCandidatesInCell(game, i, j) == 0){ continue; }
				// std::vector<int> candidates = getCandidatesInCell(game, i, j);
				for (int candidate=0; candidate<size; candidate++){
					if (game.isCandidate(i, j, candidate) == false){ continue; }
				// for (auto candidate : candidates){
					// check if two visible cells have this candidate on alternating colours
					for (int cellIndexA=0; cellIndexA<nCells; cellIndexA++){
						if (cells[cellIndexA].first != i && otherGroupShared(game, i, j, cells[cellIndexA].first, cells[cellIndexA].second) == Group::ROW){
							continue;
						}
						bool otherColourIsFirst;
						if (colouring[cellIndexA].first == candidate){
							otherColourIsFirst = false;
						} else if (colouring[cellIndexA].second == candidate){
							otherColourIsFirst = true;
						} else {
							continue;
						}
						for (int cellIndexB=cellIndexA+1; cellIndexB<nCells; cellIndexB++){
							if (cells[cellIndexB].first != i && otherGroupShared(game, i, j, cells[cellIndexB].first, cells[cellIndexB].second) == Group::ROW){
								continue;
							}
							if ((otherColourIsFirst && colouring[cellIndexB].first == candidate) || ((!otherColourIsFirst) && colouring[cellIndexB].second == candidate)){
								game.switchCandidate(i, j, candidate, false);
								if (showMove){
									if (!changed){
										if (fixedCandidate){ std::cout << "Simple Colouring"; }
										else { std::cout << "3D Medusa"; }
										std::cout << " detected involving cells ";
										for (auto cell : cells){
											std::cout << "(" << cell.first+1 << ", " << cell.second+1 << ") ";
										}
										std::cout << "\n";
									}
									printMove(i, j, candidate, false);
								}
								changed = true;
								continue;
							}
						}
					}
				}
			}
		}

		// removing candidate from cells that see itself in a colour and another candidate in the same cell in another colour
		for (int cellIndexA=0; cellIndexA<nCells; cellIndexA++){
			bool otherColourIsFirst;
			int ignoredCandidate;
			if (colouring[cellIndexA].first == game.NULL_VALUE){
				otherColourIsFirst = true;
				ignoredCandidate = colouring[cellIndexA].second;
			} else if (colouring[cellIndexA].second == game.NULL_VALUE){
				otherColourIsFirst = false;
				ignoredCandidate = colouring[cellIndexA].first;
			} else {
				continue; // should only have those two candidates, others already removed
			}
			std::vector<int> candidates = getCandidatesInCell(game, cells[cellIndexA].first, cells[cellIndexA].second);
			for (auto candidate : candidates){
				if (candidate == ignoredCandidate){ continue; }
				for (int cellIndexB=0; cellIndexB<nCells; cellIndexB++){
					if (cellIndexA == cellIndexB){ continue; }
					if ((otherColourIsFirst && colouring[cellIndexB].first == candidate) || ((!otherColourIsFirst) && colouring[cellIndexB].second == candidate)){
						if ((cells[cellIndexA].first == cells[cellIndexB].first) ||
													(otherGroupShared(game, cells[cellIndexA].first, cells[cellIndexA].second, cells[cellIndexB].first, cells[cellIndexB].second) != Group::ROW)){
							// remove candidate from cellIndexA
							game.switchCandidate(cells[cellIndexA].first, cells[cellIndexA].second, candidate, false);
							if (showMove){
								if (!changed){
									if (fixedCandidate){ std::cout << "Simple Colouring"; }
									else { std::cout << "3D Medusa"; }
									std::cout << " detected involving cells ";
									for (auto cell : cells){
										std::cout << "(" << cell.first+1 << ", " << cell.second+1 << ") ";
									}
									std::cout << "\n";
								}
								printMove(cells[cellIndexA].first, cells[cellIndexA].second, candidate, false);
							}
							changed = true;
						}
					}
				}
			}
		}
	}
	return changed;
}

bool SudokuPlayer::methodSimpleColouring(SudokuGame& game){
	for (int candidate=0; candidate<size; candidate++){
		for (int i=0; i<size; i++){
			for (int j=0; j<size; j++){
				if (!game.isCandidate(i, j, candidate)){ continue; }
				if (countCandidateInGroup(game, i, Group::ROW, candidate) == 2 ||
					countCandidateInGroup(game, j, Group::COLUMN, candidate) == 2 ||
					countCandidateInGroup(game, game.getSquare(i, j), Group::SQUARE, candidate) == 2){
					if (methodHelper3DMedusa(game, i, j, candidate, true)){
						return true;
					}
				}
			}
		}
	}
	return false;
}

bool SudokuPlayer::method3DMedusa(SudokuGame& game){
	for (int candidate=0; candidate<size; candidate++){
		for (int i=0; i<size; i++){
			for (int j=0; j<size; j++){
				if (!game.isCandidate(i, j, candidate)){ continue; }
				if (countCandidateInGroup(game, i, Group::ROW, candidate) == 2 ||
					countCandidateInGroup(game, j, Group::COLUMN, candidate) == 2 ||
					countCandidateInGroup(game, game.getSquare(i, j), Group::SQUARE, candidate) == 2){
					if (methodHelper3DMedusa(game, i, j, candidate)){
						return true;
					}
				}
			}
		}
	}
	return false;
}

bool SudokuPlayer::methodXYChain(SudokuGame& game){
	return false;
}

bool SudokuPlayer::makeMove(SudokuGame& game, bool noAssumptions){
	if (noAssumptions){
		resetCandidates(game); // ensure candidates haven't been messed with
	}
	size = game.getSize();
	// go through each candidate
	// should return true if new number is set or candidate is removed
	// removeUnnecessaryCandidates is the exception since its just housekeeping
	removeUnnecessaryCandidates(game);
	if (methodHiddenSingles(game) || methodNakedSingles(game)             || methodNakedSets(game) ||
		methodHiddenSets(game)    || methodIntersectionRemoval(game)      || methodXWing(game) || 
		methodYWing(game) || methodSwordfish(game) || methodSimpleColouring(game) ||  
		methodXYZWing(game) || methodXYChain(game) || method3DMedusa(game) || methodJellyfish(game) ||
		methodRemotePairs(game) || methodBUG(game)){
		return true;
	}

	return false;
}

bool SudokuPlayer::playGame(SudokuGame& game, bool noAssumptions){
	bool gamePlayed = false;
	if (noAssumptions){
		resetCandidates(game); // ensure candidates haven't been messed with
	}
	while (makeMove(game)){
		gamePlayed = true;
		if (showMove){
			std::cout << game;
		}
	}
	return gamePlayed;
}

std::ostream& operator<<(std::ostream& out, const SudokuPlayer::Group& g){
	switch(g){
	case (SudokuPlayer::Group::ROW):
		out << "row";
		break;
	case (SudokuPlayer::Group::COLUMN):
		out << "column";
		break;
	default:
		out << "square";
		break;
	}
	return out;
}
