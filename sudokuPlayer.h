#ifndef SUDOKU_PLAYER_H
#define SUDOKU_PLAYER_H

#include "sudokuGame.h"
#include <iostream>
#include <vector>
#include <algorithm>

class sudokuGame;

class SudokuPlayer {
public:
	enum class Strategy {
		SIMPLE, MEDIUM, DIFFICULT, MAX
	};
private:
	enum class Group {
		ROW, COLUMN, SQUARE	
	};
public:
	friend std::ostream& operator<<(std::ostream& out, const Group& g);
private:
	Strategy methods;
	bool showMove;

	int size;
	void resetCandidates(SudokuGame& game);
	int  removeCandidateFromGroup(SudokuGame& game, int groupIndex, int candidate, Group group, std::vector<int> exceptions);
	void removeCandidateFromSame(SudokuGame& game, int pos1, int pos2, int candidate);
	void removeUnnecessaryCandidates(SudokuGame& game);

	void printMove(int pos1, int pos2, int candidate, bool isSet=true) const;
	void printCandidates(SudokuGame& game) const;

	int countCandidatesInCell(SudokuGame& game, int pos1, int pos2, Group group=Group::ROW) const;
	int countCandidateInGroup(SudokuGame& game, int groupIndex, Group group, int candidate) const;
	std::vector<int> countCandidatesInGroup(SudokuGame& game, int groupIndex, Group group) const;
	std::vector<int> getIndexOfCandidateByGroup(SudokuGame& game, int groupIndex, int candidate, Group group) const;
	std::vector<int> getCandidatesInCell(SudokuGame& game, int pos1, int pos2, Group group=Group::ROW) const;
	bool isIdenticalCell(SudokuGame& game, int a, int b, int i, int j, bool cartesian=true) const;
	std::vector<int> findIdenticalInSameGroup(SudokuGame& game, int groupIndex, int index, Group group) const;

	bool methodHiddenSingles(SudokuGame& game);
	
	bool methodHelperNakedSets(SudokuGame& game, int nNaked, Group group);
	bool methodNakedSets(SudokuGame& game);
	
	bool methodHelperNakedSingles(SudokuGame& game, Group group);
	bool methodNakedSingles(SudokuGame& game);
	
	bool methodHelperHiddenSets(SudokuGame& game, int nHidden, Group group);
	bool methodHiddenSets(SudokuGame& game);

	Group otherGroupShared(SudokuGame& game, int a, int b, int i, int j, Group group=Group::ROW) const;
	int convertToNewGroupIndex(SudokuGame& game, int groupIndex, int index, Group group, Group newGroup=Group::ROW) const;
	int convertToNewGroupPos(SudokuGame& game, int groupIndex, int index, Group group, Group newGroup=Group::ROW) const;
	bool methodHelperIntersectionRemoval(SudokuGame& game, Group group);
	bool methodIntersectionRemoval(SudokuGame& game);

	bool methodHelperXWing(SudokuGame& game, int candidate, Group group, int length=2);
	bool methodXWing(SudokuGame& game);
	bool methodSwordfish(SudokuGame& game);
	bool methodJellyfish(SudokuGame& game);

	bool methodHelperYWing(SudokuGame& game, bool isXYZWing=false);
	bool methodYWing(SudokuGame& game);
	bool methodXYZWing(SudokuGame& game);

	bool methodBUG(SudokuGame& game);
	bool methodRemotePairs(SudokuGame& game);

	bool methodHelper3DMedusa(SudokuGame& game, int pos1, int pos2, int startingCandidate, bool fixedCandidate=false);
	bool methodSimpleColouring(SudokuGame& game);
	bool method3DMedusa(SudokuGame& game);
	bool methodXYChain(SudokuGame& game);
	/*
 		to Implement
 		rest of the methods from http://www.sudokuwiki.org/sudoku.htm
 		Consider reworking sudokuGame/sudokuPlayer interface
 		Make most of the conversions part of sudokuGame and have a pos struct/class
	*/

public:
	SudokuPlayer(Strategy m=Strategy::MAX, bool sM=false) : methods(m), showMove(sM) {}
	bool makeMove(SudokuGame& game, bool noAssumptions = false);
	bool playGame(SudokuGame& game, bool noAssumptions = false);
};

#endif