#include "sudokuGame.h"

void SudokuGame::resetBoard(){
	cellsLeft = size*size;
	for (int i=0; i<size; i++){
		std::fill(solvedGrid[i].begin(), solvedGrid[i].end(), NULL_VALUE);
		for (int j=0; j<size; j++){
			std::fill(candidates[i][j].begin(), candidates[i][j].end(), true);
		}
	}
}

SudokuGame::SudokuGame(int s) : size(s) {
	squareSize = sqrt(s);
	solvedGrid.resize(s);
	candidates.resize(s);
	for (int i=0; i<s; i++){
		solvedGrid[i].resize(s);
		candidates[i].resize(s);
		for (int j=0; j<s; j++){
			candidates[i][j].resize(s);
		}
	}
	resetBoard();
}

bool SudokuGame::validInd(int i) const {
	return (i >= 0 && i < size);
}

int SudokuGame::squareToPos1(int pos1, int pos2) const {
		int p1 = (pos1 / squareSize)*squareSize;
		p1 += (pos2 / squareSize);

		return p1;
}

int SudokuGame::squareToPos2(int pos1, int pos2) const {
		int p2 = (pos1 % squareSize)*squareSize;
		p2 += (pos2 % squareSize);

		return p2;
}

bool SudokuGame::isValid() const {
	for (int i=0; i<size; i++){
		// test rows
		int isUsed[size];
		for (int j=0; j<size; j++){
			isUsed[j] = false;
		}
		for (int j=0; j<size; j++){
			int value = solvedGrid[i][j];
			if (value != NULL_VALUE){
				if (isUsed[value]){
					return false;
				} else {
					isUsed[value] = true;
				}
			}
		}

		// test columns
		for (int j=0; j<size; j++){
			isUsed[j] = false;
		}
		for (int j=0; j<size; j++){
			int value = solvedGrid[j][i];
			if (value != NULL_VALUE){
				if (isUsed[value]){
					return false;
				} else {
					isUsed[value] = true;
				}
			}
		}	

		// test squares
		for (int j=0; j<size; j++){
			isUsed[j] = false;
		}
		for (int j=0; j<size; j++){
			int value = getCell(i, j, false);
			if (value != NULL_VALUE){
				if (isUsed[value]){
					return false;
				} else {
					isUsed[value] = true;
				}
			}
		}
	}
	return true;
}

bool SudokuGame::isGameDone() const{
	return cellsLeft == 0;
}

void SudokuGame::readGame(char const* fileName) {
	if (size > 9){
		throw "not designed for this :(";
	}
	std::ifstream inStream;
	inStream.open(fileName);
	resetBoard();
	int index = 0;
	char inChar;
	while (index < size*size){
		inStream.get(inChar);
		if (inChar == '.' || inChar == '0' || inChar == ' '){
			index ++;
		} else if (inChar <= '1' + size && inChar >= '1'){
			setNumber(index / size, index % size , static_cast<int>(inChar - '1'));
			index ++;
		}
	}
}

void SudokuGame::readGame(int* arr) {
	if (size > 9){
		throw "not designed for this :(";
	}
	resetBoard();
	int index = 0;
	while (index < size*size){
		if (arr[index] != 0){
			setNumber(index / size, index % size , (arr[index]-1));
		}
		index ++;
	}
}

void SudokuGame::readGame(std::vector<int> arr) {
	if (size > 9){
		throw "not designed for this :(";
	}
	resetBoard();
	int index = 0;
	while (index < size*size){
		if (arr[index] != 0){
			setNumber(index / size, index % size , (arr[index]-1));
		}
		index ++;
	}
}

int SudokuGame::getSize() const{
	return size;
}

int SudokuGame::getSquareSize() const{
	return squareSize;
}


int SudokuGame::getSquare(int pos1, int pos2) const {
	if (validInd(pos1) && validInd(pos2)){
		return (pos1/squareSize)*squareSize + (pos2/squareSize);
	} else {
		return NULL_VALUE;
	}
}

int SudokuGame::getSquareIndex(int pos1, int pos2) const {
	if (validInd(pos1) && validInd(pos2)){
		return (pos1%squareSize)*squareSize + (pos2%squareSize);
	} else {
		return NULL_VALUE;
	}
}

int SudokuGame::getColumn(int pos1, int pos2) const {
	if (validInd(pos1) && validInd(pos2)){
		return pos2;
	} else {
		return NULL_VALUE;
	}
}
int SudokuGame::getRow(int pos1, int pos2) const {
	if (validInd(pos1) && validInd(pos2)){
		return pos1;
	} else {
		return NULL_VALUE;
	}
}

int SudokuGame::getCell(int pos1, int pos2, bool cartesian) const {
	if (!cartesian){
		int p1 = squareToPos1(pos1, pos2);
		int p2 = squareToPos2(pos1, pos2);
		pos1 = p1;
		pos2 = p2;
	}

	if (validInd(pos1) && validInd(pos2)){
		return solvedGrid[pos1][pos2];
	} else {
		return NULL_VALUE;
	}
}

bool SudokuGame::isCandidate(int pos1, int pos2, int candidate, bool cartesian) const {
	if (!cartesian){
		int p1 = squareToPos1(pos1, pos2);
		int p2 = squareToPos2(pos1, pos2);
		pos1 = p1;
		pos2 = p2;
	}
	if (validInd(pos1) && validInd(pos2) && validInd(candidate)){
		return candidates[pos1][pos2][candidate];
	} else {
		//TODO : throw an error here
		return false; // ideally throw an error
	}
}


void SudokuGame::switchCandidate(int pos1, int pos2, int candidate, bool switchValue, bool cartesian){
	if (!cartesian){
		int p1 = squareToPos1(pos1, pos2);
		int p2 = squareToPos2(pos1, pos2);
		pos1 = p1;
		pos2 = p2;
	}
	if (validInd(pos1) && validInd(pos2) && validInd(candidate)){
		//if (solvedGrid[pos1][pos2] == NULL_VALUE){
			candidates[pos1][pos2][candidate] = switchValue;
		//}
	}
}

void SudokuGame::setNumber(int pos1, int pos2, int num, bool cartesian){
	if (!cartesian){
		int p1 = squareToPos1(pos1, pos2);
		int p2 = squareToPos2(pos1, pos2);
		pos1 = p1;
		pos2 = p2;
	}
	if (validInd(pos1) && validInd(pos2) && validInd(num)){
		if (solvedGrid[pos1][pos2] == NULL_VALUE){
			cellsLeft --;
		}
		solvedGrid[pos1][pos2] = num;
		for (int i=0; i<size; i++){
			candidates[pos1][pos2][i] = false;
		}
	}
}

std::string SudokuGame::exportGame() const {
	std::string s = "";
	for (int i=0; i<size; i++){
		for (int j=0; j<size; j++){
			int cellValue = getCell(i, j);
			if (cellValue == NULL_VALUE){
				s += ".";
			} else {
				s += std::to_string(cellValue+1);
			}
		}
	}
	return s;
}


std::ostream& operator<<(std::ostream& out, const SudokuGame& s ){
	out << "╔";
	for (int i=1; i<s.squareSize; i++){
		for (int j=1; j<s.squareSize; j++){
			out << "═══╤";
		}
		out << "═══╦";
	}
		for (int j=1; j<s.squareSize; j++){
			out << "═══╤";
		}
		out << "═══╗\n";

	for (int i=0; i<s.size; i++){
		out << "║ ";
		for (int j=0; j<s.size; j++){
			int cellValue = s.getCell(i, j);
			if (cellValue == s.NULL_VALUE){
				out << "  ";
			} else {
				out << cellValue+1 << " ";
			}
			if (j % s.squareSize == s.squareSize-1 ){
				out << "║ ";
			} else {
				out << "| ";
			}
		}
		if (i != s.size - 1){
			if (i % s.squareSize == s.squareSize-1 ){
				out << "\n╠";
				for (int i=1; i<s.squareSize; i++){
					for (int j=1; j<s.squareSize; j++){
						out << "═══╪";
					}
					out << "═══╬";
				}
					for (int j=1; j<s.squareSize; j++){
						out << "═══╪";
					}
					out << "═══╣\n";
			} else {
				out << "\n╟";
				for (int i=1; i<s.squareSize; i++){
					for (int j=1; j<s.squareSize; j++){
						out << "───┼";
					}
					out << "───╫";
				}
					for (int j=1; j<s.squareSize; j++){
						out << "───┼";
					}
					out << "───╢\n";
			}

		}
	}
	out << "\n╚";
	for (int i=1; i<s.squareSize; i++){
		for (int j=1; j<s.squareSize; j++){
			out << "═══╧";
		}
		out << "═══╩";
	}
		for (int j=1; j<s.squareSize; j++){
			out << "═══╧";
		}
		out << "═══╝\n";
	return out;
}
